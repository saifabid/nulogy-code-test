var calculateMarkup = function (price, person, type) {
    var proc = new workload (price, person, type);
    if (proc.error === true) {
        console.log("Error!");
        return;
    }
    proc.applyFlatMarkUp();
    proc.calculateRemainderMarkup();
    proc.applyTotalMarkup();
    return proc.Output;


};




function workload (inputPrice, inputPeople, inputType) {

    if (inputPrice === undefined || inputPeople === undefined || inputType === undefined) {
        this.error = true;
        return;
    }


    var inputPriceNum = (parseFloat(inputPrice.split("$")[1]));
    var inputPeopleNum = (parseInt(inputPeople.split(" ")[0]));
    var inputPeopleStr = inputPeople.split(" ")[1];
    if (inputPriceNum === undefined || inputPeopleNum === undefined || inputPeopleStr === undefined) {
        this.error = true;
        return undefined;
    }



    if (isNaN(inputPriceNum)) {

        this.error = true;
        return undefined;

    } else if (inputPriceNum < 0 || inputPeopleNum < 1) {
        
        this.error = true;

        return undefined;
    } else if (inputPeopleStr.toLowerCase() != "people" && inputPeopleNum > 1) {
        
        this.error = true;
        return undefined;
    } else if (inputPeopleStr.toLowerCase() != "person" && inputPeopleNum == 1) {
        
        this.error = true;
        return undefined;
    } else {
        this.Price = inputPriceNum;
        this.People = inputPeopleNum;
        this.Type = inputType;
        this.error = false;
    }
    this.applyFlatMarkUp = function() {
        this.flatMarkup = this.Price * 1.05;
    };
    this.calculateRemainderMarkup = function() {
        var workersMarkup = this.People * 1.2;
        var materialsMarkup = 0;
        if (this.Type === "food") {
            materialsMarkup = 13;
        } else if (this.Type === "drugs") {
            materialsMarkup = 7.5;
        } else if (this.Type === "electronics") {
            materialsMarkup = 2;
        } else {
            materialsMarkup = 0;
        }
        this.RemainderMarkup = workersMarkup + materialsMarkup;
    };
    this.applyTotalMarkup = function() {

        this.Output = Math.round((this.flatMarkup * (1 + this.RemainderMarkup / 100)) * 100) / 100;
        this.Output = "$".concat(this.Output.toString());
        return this.Output;

    };


};

exports.calculateMarkup = calculateMarkup;