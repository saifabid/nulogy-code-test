describe("Given test cases", function() {

    var price;
    var people;
    var type;


    it("3 people and food", function() {
        price = "$1299.99";
        people = "3 people";
        type = "food";
        var proc = new workload(price, people, type);
       
        proc.applyFlatMarkUp();
        proc.calculateRemainderMarkup();
        proc.applyTotalMarkup()
        

        expect(proc.Output).toEqual("$1591.58");
    });
    it("1 person and drugs", function() {
        price = "$5432.00";
        people = "1 person";
        type = "drugs";
        var proc = new workload(price, people, type);
       
        proc.applyFlatMarkUp();
        proc.calculateRemainderMarkup();
        proc.applyTotalMarkup()
        

        expect(proc.Output).toEqual("$6199.81");
    });
    it("4 people and books", function() {
        var proc = new workload("$12456.95", "4 people", "books");
        
        proc.applyFlatMarkUp();
        proc.calculateRemainderMarkup();
        proc.applyTotalMarkup()
        

        expect(proc.Output).toEqual("$13707.63");
    });

});
describe("Errors in pricing input", function() {

    var price;
    var people;
    var type;


    it("price is negative", function() {
        price = "$-10";
        people = "3 people";
        type = "food";
        var proc = new workload(price, people, type);
        expect(proc.error).toBe(true);
    });
    it("Incorrectly formatted price input", function() {
        price = "faultyPRICE";
        people = "3 people";
        type = "food";
        var proc = new workload(price, people, type);
        expect(proc.error).toBe(true);


    });
    it("Undefined price", function() {
        price = undefined;
        people = "3 people";
        type = "food";
        var proc = new workload(price, people, type);
        expect(proc.error).toBe(true);


    });

});
describe("Errors in the worker input", function() {

    var price;
    var people;
    var type;


    it("Less than 0 people", function() {
        price = "$1299.99";
        people = "-1 people";
        type = "food";
        var proc = new workload(price, people, type);
        expect(proc.error).toBe(true);
    });
    it("0 people", function() {
        price = "1299.99";
        people = "0 people";
        type = "food";
        var proc = new workload(price, people, type);
        expect(proc.error).toBe(true);


    });
    it("3 person (incorrect notation)", function() {
        price = "$1299.99";
        people = "3 person"
        type = "food";
        var proc = new workload(price, people, type);
        expect(proc.error).toBe(true);


    });
       it("1 people (incorrect notation)", function() {
        price = "$1299.99";
        people = "1 people"
        type = "food";
        var proc = new workload(price, people, type);
        expect(proc.error).toBe(true);


    });
        it("No number of workers supplied", function() {
        price = "$1299.99";
        people = "person"
        type = "food";
        var proc = new workload(price, people, type);
        expect(proc.error).toBe(true);


    });

    it("Undefined people", function() {
        price = "$1299.99";
        people = undefined
        type = "food";
        var proc = new workload(price, people, type);
        expect(proc.error).toBe(true);


    });

});

describe("Errors in the type input", function() {

    var price;
    var people;
    var type;


    it("Undefined type", function() {
        price = "$1299.99";
        people = "2 people"
        type = undefined;
        var proc = new workload(price, people, type);
        expect(proc.error).toBe(true);


    });

});